import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/net/http_urls.dart';
import 'package:flutter_baselib_example/module/login/model/login_result_bean.dart';
import 'package:flutter_baselib_example/module/userlist/model/user_detail_bean.dart';

///@date:  2021/3/2 11:26
///@author:  lixu
///@description: 获取token viewModel
///
///进入View页面时，需要http获取数据后才能显示UI的场景继承[BaseViewModel]类，配合[BaseView]使用
///泛型[UserDetailBean]：进入页面时，http获取数据对象的类型
class TokenViewModel extends BaseViewModel<LoginResultBean> {
  TokenViewModel() : super(isRequestListData: false);

  ///获取http请求参数
  @override
  Map<String, dynamic> getRequestParams() {
    return {
      'account': '15015001500',
      'pass': '123qwe',
      'appType': 'PATIENT',
      'device': 'ANDROID',
      'push': '13065ffa4e22e63efd2',
    };
  }

  @override
  String getTag() {
    return 'TokenViewModel';
  }

  ///获取http请求url
  @override
  String getUrl() {
    return HttpUrls.loginUrl;
  }
}
