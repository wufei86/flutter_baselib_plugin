import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:flutter_baselib_example/common/util/ui_utils.dart';
import 'package:flutter_baselib_example/module/login/view_model/login_view_model.dart';

///@date:  2021/3/1 12:00
///@author:  lixu
///@description: 登录页
class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('登录页'),
      ),
      body: ChangeNotifierProvider(
        create: (_) {
          return LoginViewModel();
        },
        child: Consumer<LoginViewModel>(
          builder: (context, viewModel, child) {
            return Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  UIUtils.getLoginPageDemoDescWidget(),
                  _getLoginInfoWidget(viewModel),
                  UIUtils.getButton('登录接口\n（演示：http获取单个数据对象）', () {
                    viewModel.onLogin(context);
                  }),
                  UIUtils.getButton('用户列表接口\n（演示：http获取List数据对象）', () {
                    viewModel.getUserList(context);
                  }),
                  UIUtils.getButton('最简单的http请求测试\n（3行代码实现）', () {
                    viewModel.simplestHttpDemo(context);
                  }),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  ///获取登录信息widget
  _getLoginInfoWidget(LoginViewModel viewModel) {
    String loginInfo = '是否登录：${viewModel.loginResultBean != null}\n'
        '用户ID：${viewModel.loginResultBean?.user?.userId}\n'
        '用户名：${viewModel.loginResultBean?.user?.name}\n';

    return Visibility(
      visible: viewModel.loginResultBean != null,
      child: Text(loginInfo),
    );
  }
}
