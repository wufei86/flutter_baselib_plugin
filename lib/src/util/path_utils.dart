import 'dart:io';

import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/3/3 11:45
///@author:  lixu
///@description: 文件路径获取工具
class PathUtils {
  PathUtils._();

  ///获取临时文件夹，针对于 Android 设备 getCacheDir()
  ///和 iOS 设备 NSTemporaryDirectory() 返回的值
  static Future<Directory> getCacheDir() async {
    return getTemporaryDirectory();
  }

  /// 获取应用文件目录(IOS和安卓通用)
  /// 用于放置用户生成的数据或不能由应用程序重新创建的数据 用户不可见
  /// 获取 Document 文件夹，针对 Android 设备的 AppData 目录，
  /// iOS 设备的 NSDocumentDirectory 目录
  static Future<Directory> getApplicationDocumentsDir() async {
    return getApplicationDocumentsDirectory();
  }

  ///获取存储卡目录，只有 Android 设备可用
  static Future<Directory?> getExternalStorageDir() async {
    if (Platform.isAndroid) {
      return getExternalStorageDirectory();
    } else {
      return getCacheDir();
    }
  }
}
