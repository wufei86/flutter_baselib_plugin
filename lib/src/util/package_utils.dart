import 'package:package_info_plus/package_info_plus.dart';

///@date:  2021/2/24
///@author:  lixu
///@description: 包管理工具
class PackageUtils {
  PackageUtils._();

  static String _versionName = '';
  static String _appName = '';
  static String _packageName = '';
  static String _buildNumber = '';

  static Future<String> getVersionName() async {
    await _initPackage();
    return _versionName;
  }

  static Future<String> getAppName() async {
    await _initPackage();
    return _appName;
  }

  static Future<String> getPackageName() async {
    await _initPackage();
    return _packageName;
  }

  static Future<String> getBuildNumber() async {
    await _initPackage();
    return _buildNumber;
  }

  static _initPackage() async {
    if (_versionName.length == 0) {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      _appName = packageInfo.appName;
      _packageName = packageInfo.packageName;
      _versionName = packageInfo.version;
      _buildNumber = packageInfo.buildNumber;
    }
  }
}
