import 'package:flutter/material.dart';

///@date:  2021/3/1 13:58
///@author:  lixu
///@description: 加载中，占位view
class LoadingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(
          Colors.blue,
        ),
      ),
    );
  }
}
