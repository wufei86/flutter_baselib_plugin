import 'package:flutter/material.dart';

///@date:  2021/3/1 14:14
///@author:  lixu
///@description: http加载状态父view
class LoadStateParentView extends StatelessWidget {
  final Widget _child;
  final VoidCallback? _callback;

  LoadStateParentView(this._child, this._callback);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        _callback?.call();
      },
      child: Center(child: _child),
    );
  }
}
